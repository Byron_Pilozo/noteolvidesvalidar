#Los números romanos aún son utilizados para algunos propósitos. Los símbolos básicos y sus equivalencias
#decimales son:
#M 1000
#D 500
#C 100
#L 50
#X 10
#V 5
#I 1
#Los enteros romanos se escriben de acuerdo a las siguientes reglas:
#a) Si una letra está seguida inmediatamente por una de igual o menor valor, su valor se suma al total
#acumulado. Así, XX = 20, XV = 15 y VI = 6.
#b) Si una letra está seguida inmediatamente por una de mayor valor, su valor se sustrae del total
#acumulado. Así, IV = 4, XL = 40 y CM = 900.
#Escriba un programa que reciba un string con un número en notación romana, y entregue el entero
#equivalente a arábigo. Ejemplos:
#Romano MCMXIV Arabigo: 1914
#Romano XIV Arabigo: 14
#Romano X Arabigo: 10
#Romano IV Arabigo: 4

# numero_romano debe ser un String
def romano_a_arabigo(numero_romano):
    # Resultado de la transformacion
    resultado = 0

    # Usamos un diccionario, ya que se adapta al concepto
    # de que a cada letra le corresponde un valor
    valores = {
        'M' : 1000,
        'D' : 500,
        'C' : 100,
        'L' : 50,
        'X' : 10,
        'V' : 5,
        'I' : 1
    }

    if len(numero_romano) > 0:
        # Con esto, siempre sumamos el primer numero
        valor_anterior = 0

    # Por cada letra en el numero romano (string)
    for letra in numero_romano:

        # Si la letra se encuentra en el diccionario
        if letra in valores:
            # Obtenemos su valor
            valor_actual = valores[letra]
        else:
            # Si no, la letra es invalida
            print ('Valor invalido:'), letra
            return 'no es un acotación romana' # NaN: Not A Number

        # Si el valor anterior es mayor o igual que el
        # valor actual, se suman
        if valor_anterior >= valor_actual:
            resultado += valor_actual
        # Si no, se restan
        else:
            # Esto equivale a:
            # resultado = (resultado - valor_anterior) + (valor_actual - valor_anterior)
            resultado += valor_actual - (2 * valor_anterior)

        # El valor actual pasa a ser el anterior, para analizar
        # la siguiente letra en el numero romano
        valor_anterior = valor_actual

    # Al terminar, retorna el numero resultante
    return resultado

# Con upper() la letra siempre sera pasada a mayuscula
numero_romano = str(input("Numero Romano: ")).upper()
print ("Numero Arabigo:", romano_a_arabigo(numero_romano))