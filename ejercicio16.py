# Crear un programa que solicite el ingreso de un valor entero e indique si dicho valor es primo o no.


while True:
    try:
        valor = int(input("Ingrese un número mayor que 1:\n"))# permite ingresar un número mayor que 1 en la variable valor
        if valor <= 1: # evalua  que el numero insertado sea mayor a uno.
            print("POR FAVOR INGRESE UN NUMERO MAYOR QUE 1\n")
            # imprime cuando usted ingresa uno pidiendo que ingrese un valor que no se uno.
        else:
            c = 0 # creamos la variable c yvque inicie desde cero
            for i in range(1, valor):# evalua el bucle retornando al valor insertado
                if valor % i == 0:#evalua que el valor insertado
                     c = c + 1# el contador e incremento
            if c == 1:# evalua la variable c
                print("El número" , valor ,"es primo.".format(valor))# imprime el valor cuando es el numero es primo.
            else:
                print("El número" , valor , "no es primo.".format(valor))# imprime el valor cuando es el numero no es primo.
    except ValueError:
        print("No ingreso un numero","Ingrese un numero valido")# le notifica que ingrese un valor incorrecto.