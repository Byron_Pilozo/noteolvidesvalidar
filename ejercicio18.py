#En cada ronda del juego del cachipún, los dos competidores deben elegir entre jugar tijera, papel o piedra. Las
#reglas para decidir quién gana la ronda son: tijera le gana a papel, papel le gana a piedra, piedra le gana a tijera,
#y todas las demás combinaciones son empates. El ganador del juego es el primero que gane tres rondas.
#Escriba un programa que pregunte a cada jugador cuál es su jugada, muestre cuál es el marcador después de
#cada ronda, y termine cuando uno de ellos haya ganado tres rondas. Los jugadores deben indicar su jugada
#escribiendo: tijera, papel o piedra.

# Puntos del jugador A y B.
puntos_a = 0
puntos_b = 0
# Definir al jugador A y B.
jugador_A = 0
jugador_B = 0
#pregunta puntos_a es menor que tres y puntos_b es menor que tres
while puntos_a < 3 and puntos_b < 3:
    letras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't',
              'u', 'v', 'w', 'x', 'y', 'z']
    while True:
        jugador_A = input('Competidor A, solo ingrese tijera, papel o piedra:').lower() # ingrese competidor A
        verificacion = True
        for elemento in jugador_A:
            if elemento not in letras:
                print(' no se ingreso tijera, papel o piedra.')
                verificación = False
                break  # Rompe directamente el bucle en cuanto encuentra un elemento que no es una letra
        if verificacion == True:
            reversed(jugador_A)
        jugador_B = input('Competidor B, solo ingrese tijera, papel o piedra:')# ingrese competidor B
        verificacion = True
        for elemento in jugador_B:
            if elemento not in letras:
                print(' no se ingreso tijera, papel o piedra.')
                verificacion = False
                break
        if verificacion == True:
            reversed(jugador_B)

        # Combinaciones del juego para que gane el jugador A.
        if (jugador_A == 'tijera' and jugador_B == 'papel') or (jugador_A == 'piedra' and jugador_B == 'tijera') or (
                jugador_A == 'papel' and jugador_B == 'piedra'):
            puntos_a = puntos_a + 1
            print('A:', puntos_a, '-', 'B:', puntos_b)


        # Combinaciones del juego para que gane el jugador B.
        elif (jugador_A == 'piedra' and jugador_B == 'papel') or (jugador_A == 'papel' and jugador_B == 'tijera') or (
                jugador_A == 'tijera' and jugador_B == 'piedra'):
            puntos_b = puntos_b + 1
            print('A:', puntos_a, '-', 'B:', puntos_b)

        # Marcador de cada ronda.
        elif jugador_A == jugador_B:
            puntos_a = puntos_a
            puntos_b = puntos_b
            print('A:', puntos_a, '-', 'B:', puntos_b)

        # Combinaciones del juego para que haya un empate entre el jugador A y B.
        else:
            (jugador_A == 'piedra' and jugador_B == 'piedra') or (jugador_A == 'papel' and jugador_B == 'papel') or (
                jugador_A == 'tijera' and jugador_B == 'tijera')
            puntos_a = puntos_a + 0
            puntos_b = puntos_b + 0
            print('A:', puntos_a, '-', 'B:', puntos_b)

    # Si el jugador A gana 3 rondas.
        if puntos_a == 3:
            print('El jugaor A es el ganador.')
        # Caso contrario, si el jugador B gana 3 rondas, es el ganador.
        elif puntos_b == 3:
            print('El jugador B es el ganador.')