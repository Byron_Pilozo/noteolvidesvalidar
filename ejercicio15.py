#Un número natural es un palíndromo si se lee igual de izquierda a derecha y de derecha a izquierda. Por
#ejemplo, 14941 es un palíndromo, mientras que 751no lo es. Escriba un programa que indique si el número
#ingresado es o no palíndromo.
while True:
    n= input('Inserte un numero que crea usted que es palindromo: ')# permite ingresa un valor en n
    try:
        val = int(n) # guarda el numero ingresado
        if n == str(n)[::-1]: # evalúa el numero ingresado
            print("Correcto el número" ,n , "que inserto es Palíndromo...".format(n))
            # imprime correcto cuando el valor insertado en n es un palíndromo.
        else:
            print("Incorrecto lo sentimos el número" , n , "que inserto no es un palíndromo...".format(n))
            # imprime incorrecto cuando el valor insertado en n  no es un palíndromo.
    except ValueError:
        print("Ingrese un numero valido")# le notifica que ingrese un valor palíndromo correcto.