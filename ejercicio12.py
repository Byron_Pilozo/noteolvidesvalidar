
#Escriba un programa que muestre los números naturales menores o iguales que un número n determinado,
#que no sean múltiplos ni de 3 ni de 5.
# definimos un rango retornando en n, dependiendo del valor n insertado retorna hasta completar su bucle.
while True:
    try:
        n = int(input("Inserte un número: "))#permite insertar un numero en la variable n.
        break
    except ValueError:
        print('no es un numero natural')# imprime el error
#n = 10 #dejamos el 10 si no se desea inserta un número.
for n in range(1,n+1): # definimos un rango retornando en n, dependiendo del valor n insertado retorna hasta completar su bucle.
   if n%3!=0 and n%5!=0:# evalúa el valor insertado en la variable n para que no muestre los múltiplos ni de 3 ni de 5
     print (n)# imprime el bucle hasta su final, a excepción de los múltiplos ni de 3 ni de 5
