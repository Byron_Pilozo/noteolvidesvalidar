#Escriba un programa que entregue la edad del usuario (Día, Mes y Año) a partir de su fecha de nacimiento.
#Ingrese usted el año actual y considere las validaciones posibles
# Para obtener la fecha actual, puede hacerlo usando la función localtime que viene en el módulo time.
from time import localtime

fecha_actual = localtime()
fecha_actual_dia = fecha_actual.tm_mday
fecha_actual_mes = fecha_actual.tm_mon
fecha_actual_año = fecha_actual.tm_year
# Primero pedimos la fecha de nacimiento del usuario.
print("Ingrese su fecha de nacimiento.")
dia_de_nacimiento = int(input("Ingrese el día de nacimiento: "))
mes_de_nacimiento = int(input("Ingrese el mes de nacimiento: "))
año_de_nacimiento = int(input("Ingrese el año de nacimiento: "))

 # Ahora el programa va a entregar la edad del usuario y también la fecha actual (Día, Mes y Año).
if fecha_actual_mes > mes_de_nacimiento or (
fecha_actual_mes == mes_de_nacimiento and fecha_actual_dia >= dia_de_nacimiento):
    print("Usted tiene", fecha_actual_año - año_de_nacimiento, "años hasta la fecha actual: ", fecha_actual_dia,
              "/", fecha_actual_mes, "/", fecha_actual_año)
else:
    print("Usted tiene", fecha_actual_año - año_de_nacimiento - 1, "años hasta la fecha actual: ", fecha_actual_dia,
                "/", fecha_actual_mes, "/", fecha_actual_año)

