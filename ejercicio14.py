#Un jugador debe lanzar dos dados numerados de 1 a 6, y su puntaje es la suma de los valores obtenidos. Un
#puntaje dado puede ser obtenido con varias combinaciones posibles. Por ejemplo, el puntaje 4 se logra con las
#siguientes tres combinaciones: 1+3, 2+2 y 3+1. Escriba un programa que pregunte al usuario un puntaje, y
#muestre como resultado la cantidad de combinaciones de dados con las que se puede obtener ese puntaje.
while True:
    c = 0  # definimos la variable cen cero
    try:
        puntaje = int(input("Ingrese el puntaje: "))# permite ingresar un puntaje
    except ValueError:
        print('no ingreso un puntaje')#imprime el mensaje de error
    for i in range(1,7):# permite solo recorrer siete veces por el rango establecido  en i que es siete.
        for j in range(1,7):# permite solo recorrer siete veces por el rango establecido en j que es siete.
            if i+j == puntaje: # evalua en una las combinacione igualando con el puntaje.
                c += 1 # contador de combinaciones.
    print ("Hay", c, "combinaciones para obtener", puntaje, "puntos.")# imprime las combinaciones
#Ingrese el puntaje: 4
#Hay 3 combinaciones para obtener 4
#Ingrese el puntaje: 11
#Hay 2 combinaciones para obtener 11
#Ingrese el puntaje: 17
#Hay 0 combinaciones para obtener 17