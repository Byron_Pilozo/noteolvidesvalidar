#El colegio ABC mundo ha instaurado un nuevo reglamento de evaluaciones. Todas las asignaturas deben tener
#tres certámenes y un examen. Las notas van entre 0 y 10, con un decimal.
#Después de los tres certámenes, los alumnos con promedio menor que 3 reprueban y los con promedio mayor
#o igual que 7 aprueban. El resto va al examen, en el que deben sacarse por lo menos un 5 para aprobar.
#Además, para reducir el trabajo de los profesores, se decidió que los alumnos que se sacan menos de un 2 en
#los dos primeros certámenes están automáticamente reprobados. A su vez, los que obtienen más de un 9 en
#los dos primeros certámenes están automáticamente aprobados. En ambos casos, no deben rendir el tercer
#certamen.
#Escriba un programa que pregunte a un estudiante las notas de las evaluaciones que rindió, y le diga si está
#aprobado o reprobado.
nota_del_primer_parcial = float(input("Ingrese nota del primer parcial: "))# ingresa la nota de primer parcial
nota_del_segundo_parcial = float(input("Ingrese nota del segundo parcial: "))# ingresa la nota de segundo parcial

if 2 <= (nota_del_segundo_parcial + nota_del_primer_parcial) / 2 <= 9:
#pregunta 2 es mayor que (nota_del_segundo_parcial + nota_del_primer_parcial)
#dividida para 2 que es mayor igual a 9
    nota_del_tercer_parcial = float(input("Ingrese nota del tercer parcial: "))
#permite la nota de tercer parcial
    if (nota_del_primer_parcial + nota_del_segundo_parcial + nota_del_tercer_parcial) / 3 < 3:
        print("El estudiante reprobo.")
#pregunta (nota_del_primer_parcial + nota_del_segundo_parcial + nota_del_tercer_parcial)) que
#dividida para 3 que es mayor igual a 3
    elif (nota_del_segundo_parcial + nota_del_primer_parcial + nota_del_tercer_parcial) / 3 >= 7:
        print("El estudiante aprobo.")
#pregunta (nota_del_segundo_parcial + nota_del_primer_parcial + nota_del_tercer_parcial) que
#dividida para 3 que es mayor igual a 7
    else:
        examen = float(input("Ingrese la nota del examen: "))
#ingrese nota de examen
        if examen < 5: # pregunta examen es mayo que 5
            print("El estudiante reprobo.") # imprime si el estudiante reprobo

        else:
            print("El estudiante aprobo")# imprime si el estudiante  aprobo

elif (nota_del_segundo_parcial + nota_del_primer_parcial) / 2 < 2:
    print("El estudiante reprobo.")
#pregunta (nota_del_segundo_parcial + nota_del_primer_parcial) que
#dividida para 2 que es mayor igual a 2

elif (nota_del_segundo_parcial + nota_del_primer_parcial) / 2 >= 9:
    print("El estudiante aprobo.")
#pregunta (nota_del_segundo_parcial + nota_del_primer_parcial) que
#dividida para 2 que es mayor igual a 9