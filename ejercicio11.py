# Variable para el tiempo total de viaje
tiempo_total = 0
tramo_duracion = 1
# Comenzamos pidiendo una duracion de tramo
while tramo_duracion != 0:# Mientras la duracion no sea cero ...
        try:
                tramo_duracion = int(input('Duracion tramo en minutos: '))
                #tramo = int(input("Ingrese la duracion del tramo en minutos:"))
                #break
        except ValueError:
                print (" No es un numero entero")#impreme un mensaje de error

        tiempo_total = tiempo_total + tramo_duracion #sumamos los tramos de duracion a nuestro contador
        horas = tiempo_total / 60 # calculamos en las horas
        minutos = tiempo_total % 60 # calculamos los minutos
# Cuando la duracion ingresada sea cero, expresamos
print ('Tiempo total de viaje: %02d:%02d horas' % (horas, minutos))# el tiempo total en horas y minutos

#Ejemplo de tramo_duracion
#Duración tramo: 15
#Duración tramo: 30
#Duración tramo: 87
#Duración tramo: 0
#Tiempo total de viaje: 2:12 horas
#Duración tramo: 51
#Duración tramo: 17
#Duración total de viaje: 1:08 horas
